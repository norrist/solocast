# What is solocast

### Record a spoken text, paragraph by paragraph.

- An easy to use python script to record short segments of your audio.
- Removes noise from your device.
- HPR [Episode](https://hackerpublicradio.org/eps/hpr3496/) about `solocast`

## Howto

- Install [sox](https://sox.sourceforge.net/Main/Links)
- Install solocast by either:
  - pipx install solocast
  - pip install solocast
  - Download solocast.py, make it executable, and place it in your $PATH
- Write a podcast script (paragraphs/keywords/cues) and save it as `script.txt` or `script.md`

## Script Format

- `script.txt` - Text segments are split at **empty lines**
- `script.md` - Text segments are split at **markdown headers**

```python
MARKDOWN_HEADERS = (
    "# ",
    "## ",
    "### ",
    "#### ",
    "--- ",
)
```

## solocast commands and functions

Open a terminal in the location of solocast.py.

`solocast.py --help`

- List all commands

`solocast.py review`

- Review your text, and verify solocast divides your script as expected.

`solocast status`

- Print number of segments Recorded and Remaining.
- Check if the noise profile and combined recording exists.

`solocast record`

- presents segments script in small chunks
- for each segment
  - record the segment audio
  - Playback, Accept, Truncate silence, Record again

`solocast combine`

Run when all the segments are recorded

- Combine the segments
- Apply the noise reduction filter
- Truncate silence

`solocast export`

- Optional step
- Create the combined file if it does not exist.
- Export the combined recording as flac.

## TODO

- Add option to append audio to an existing recording
- Migrate sox commands to pysox
- Better doc strings
- Better processing of combined audio
  - remove loud noise artifacts like clicks
- Evaluate other recording and sound processing methods
  - ffmpeg
  - GUI audio recording app.
- Better tests
- Try something keep CRTL-c from killing Python while recording
  on OpenBSD
