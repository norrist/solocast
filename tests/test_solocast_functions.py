import os
import importlib
import shutil

import pytest

# from click.testing import CliRunner
import solocast.solocast
from solocast.solocast import FORMAT, RECORDING_DIRECTORY


@pytest.fixture(autouse=True)
def no_input(monkeypatch):
    monkeypatch.setattr("builtins.input", lambda a: a)


def create_script(format="txt"):
    shutil.copyfile("sample_script.txt", f"script.{format}")
    yield f"script.{format}"
    os.remove(f"script.{format}")


test_slug = "RandomTextMultilineMarkdownSymb"
test_text = """
Random Text
Multiline
### Markdown symbols
- bullet
- bullet
"""
txt_fmt_slugs = (
    "BulletedListsAreOkButKeepTheItem",
    "MarkdownFormattingIsOkButThe",
    "ThisIsASampleScriptForSolocast",
)

md_fmt_slugs = (
    "MarkdownFormattingIsOkButThe",
    "ThisIsASampleScriptForSolocast",
)
test_recording = f"{RECORDING_DIRECTORY}/{test_slug}.{FORMAT}"


def test_with_md():
    importlib.reload(solocast)
    for test_file_name in create_script(format="md"):
        solocast.solocast.load_script()
        segments = solocast.solocast.script_segments
        print(segments.keys())
        assert len(segments) == 2

        assert solocast.solocast.script_file_name() == test_file_name
        for testslug in md_fmt_slugs:
            assert testslug in segments.keys()


def test_with_txt():
    for test_file_name in create_script(format="txt"):
        solocast.solocast.load_script()
        segments = solocast.solocast.script_segments

        assert solocast.solocast.script_file_name() == test_file_name
        assert len(segments) == 4
        for testslug in txt_fmt_slugs:
            assert testslug in segments.keys()


def test_variables():
    assert RECORDING_DIRECTORY == "Recordings"
    assert FORMAT == "wav"


def test_get_recording_name():
    assert (
        solocast.solocast.get_recording_file_name(test_slug)
        == f"{RECORDING_DIRECTORY}/{test_slug}.{FORMAT}"
    )


def test_project_prep():
    solocast.solocast.project_prep()
    assert os.path.exists(RECORDING_DIRECTORY)


def test_add_slug_text():
    solocast.solocast.add_slug_text(test_text)
    assert solocast.solocast.script_segments[test_slug] == test_text


def test_recording_exists():
    os.mknod(test_recording)
    assert solocast.solocast.recording_exists(test_slug) is True
    os.remove(test_recording)
    assert solocast.solocast.recording_exists(test_slug) is False


def test_noise_profile_missing():
    """
    Assume a noise file exists.
    Try to create a noise file.
    If noise file was created by pytest:
      Set Variable to True.
      Delete and test the noise file is missing.
    """
    noise_file_created_by_test = False
    try:
        os.mknod(f"{RECORDING_DIRECTORY}/noise.prof")
        noise_file_created_by_test = True
    except FileExistsError:
        pass
    assert solocast.solocast.noise_profile_missing() is False
    if noise_file_created_by_test:
        os.remove(f"{RECORDING_DIRECTORY}/noise.prof")
        assert solocast.solocast.noise_profile_missing() is True


def test_wait_for_input(capsys):
    solocast.solocast.wait_for_input()
    captured = capsys.readouterr()
    assert captured.out == f"{'*'*40}\n"
